<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Models\Buyer;
use App\Models\Order;
use App\Models\OrderItems;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        
        return $orders->map(function ($order){
            return new OrderResource($order);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $order = new Order();
        
        if (isset($data['order_items'])) {
            $orderItems = OrderItems::create($data['order_items']);
            $orderItems->save();
            $order->order_items_id = $orderItems->id;
    
        }

        if (isset($data['buyer'])) {
            $buyer = Buyer::create($data['buyer']);
            $buyer->save();
            $order->buyer_id = $buyer->id;
        }

        $order->date = $data['date'] ?? null;


        
        $order->save();
        
        return new OrderResource($order);
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        if (!$order) {
            return new Response([
                'result' => 'fail',
                'message' => 'order not found'
            ]);
        }

        $data = $request->all();

        $order->update($data);
        $order->save();

        return new Response(new OrderResource($order));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Order::destroy($id);
        return ['result' => $result ? 'success' : 'fail'];
    }
}
