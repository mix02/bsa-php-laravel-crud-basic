<?php
namespace App\Presenter;

use Illuminate\Database\Eloquent\Model;

class OrderPresenter
{

    public function __construct(protected Model $model)
    {
    
    }


    public function fullName()
    {
        return trim($this->model->name . ' ' . $this->model->surname);
    }


    public function phone()
    {
        return $this->model->phone;
    }


    public function fullAddress()
    {
        return trim($this->model->country . ' ' . $this->model->city . ' ' . $this->model->addressLine);
    }
}