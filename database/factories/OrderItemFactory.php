<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => Product::all()->random()->id,
            'order_id' => Order::all()->random()->id,
            'quantity' => $this->faker->numberBetween(3, 15),
            'price' => $this->faker->randomFloat(1000),
            'discount' => $this->faker->numberBetween(10, 100),
            'sum' => $this->faker->numberBetween(40, 150)
        ];
    }
}
