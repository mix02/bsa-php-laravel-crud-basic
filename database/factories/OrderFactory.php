<?php

namespace Database\Factories;

use App\Models\Buyer;
use App\Models\OrderItems;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            
            'date' => now()->addDay(2),
            'order_items_id' => OrderItems::all()->random()->id,
            'buyer_id' => Buyer::all()->random()->id
        ];
    }
}
