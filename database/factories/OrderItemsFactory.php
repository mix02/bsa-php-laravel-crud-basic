<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $id = Product::all()->random()->id;
        $product = Product::where('id', $id)->first();
        return [
            'product_id' => $product->id,
            'product_discount' => $this->faker->numberBetween(10, 100) / 100 * $product->price,
            'product_name' => $product->name,
            'product_quantity' => $this->faker->numberBetween(10, 30),
            'product_price' => $product->price,
            'product_sum' => $product->price + $this->faker->randomNumber(3)
        ];
    }
}
