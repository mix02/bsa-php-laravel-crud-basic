<?php

namespace Database\Seeders;

use App\Models\Seller;
use Database\Factories\ProductFactory;
use Database\Factories\SellerFactory;
use Faker\Factory;
use Illuminate\Database\Seeder;

class SellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        (new SellerFactory(10))->create()
            ->each(function ($seller)  use ($faker) {
                $seller->products()->saveMany(
                    (new ProductFactory(10))->make(['seller_id'=> Seller::all()->random()->id])
                );
            });
    }
}
