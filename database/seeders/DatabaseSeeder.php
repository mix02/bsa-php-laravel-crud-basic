<?php

namespace Database\Seeders;

use App\Models\OrderDetail;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ProductSeeder::class,
            SellerSeeder::class,
            BuyerSeeder::class,
            OrderItemsSeeder::class,
            OrderSeeder::class,
            OrderItemSeeder::class,
            OrderDetailSeeder::class
        ]);
    }
}
