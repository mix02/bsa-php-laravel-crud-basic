<?php

namespace Database\Seeders;

use App\Models\Buyer;
use Database\Factories\BuyerFactory;
use Illuminate\Database\Seeder;

class BuyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Buyer::factory()->count(20)->create();
    }
}
