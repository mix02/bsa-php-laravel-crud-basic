<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_id');
            $table->double('product_discount')->nullable();
            $table->string('product_name');
            $table->integer('product_quantity')->nullable();
            $table->double('product_price')->nullable();
            $table->integer('product_sum')->nullable();
            $table->timestamps();

            $table
                ->foreign('product_id')
                ->references('id')
                ->on('my_products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
